#![macro_use]

use crate::parser::lexer::Lexer;
use crate::parser::tokens::Tokens;

use std::fs::File;
use std::io;
use std::io::BufReader;
use std::io::Read;

pub fn read_file(path: &str) -> io::Result<Vec<u8>> {
    let f = File::open(path)?;
    let mut reader = BufReader::new(f);
    let mut buffer = Vec::new();

    // Read file into vector.
    reader.read_to_end(&mut buffer)?;
    Ok(buffer)
}

pub fn parse_file(path: &str) -> Result<Tokens, String> {
    match read_file(path) {
        Ok(text) => {
            let l = Lexer::new();
            match l.tokenize(&text) {
                Ok(tokens) => Ok(tokens),
                Err(e) => Err(e.to_string()),
            }
        }
        Err(e) => Err(e.to_string()),
    }
}
