pub fn invalid_var(row: usize, col: usize) -> String {
    format!("invalid variable starting from {row}:{col}")
}

pub fn unclosed_quote(row: usize, col: usize) -> String {
    format!("unclosed quote starting from {row}:{col}")
}
pub fn unclosed_block(row: usize, col: usize) -> String {
    format!("unclosed block starting from {row}:{col}")
}
