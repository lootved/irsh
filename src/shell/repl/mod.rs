use std::io::{self, Write};
use std::time::Instant;

use super::cmd::run_cmd_line;
use tty::{Key, RawTTy};

pub mod colors;
pub mod history;
pub mod tty;

mod prompt;

macro_rules! log {
    ($x:ident, $($tts:tt)*) => {
        if cfg!(debug_assertions){
        _ = writeln!($x.debug_file, $($tts)*);
        }
    }
}
pub(crate) use log;

pub fn run() {
    let repl = Repl::new();
    repl.main_loop();
    tty::reset_terminal_mode();
}

impl Repl {
    fn main_loop(mut self) {
        let prompt = prompt::Prompt::new();

        let mut stdout = io::stdout().lock();
        let mut elapsed: u64 = 0;
        loop {
            //infinite loop
            prompt.show(&self.state.pwd, elapsed);
            loop {
                // loop to read one command
                match self.tty.read_one_key() {
                    Key::Enter => {
                        let start = Instant::now();
                        self.run_cmd();
                        elapsed = start.elapsed().as_secs();
                        break;
                    }
                    Key::Tab => self.handle_completion(),
                    Key::Up => {
                        let line = self.state.hist.get_previous();
                        if !line.is_empty() {
                            if let Ok(s) = std::str::from_utf8(&self.buffer) {
                                log!(self, "buffer when click up: {}", s);
                            }
                            log!(self, "2){}:{}", self.position_on_line, line);

                            self.clear_line_and_buffer();
                            self.display_line(&line);

                            if let Ok(s) = std::str::from_utf8(&self.buffer) {
                                log!(self, "buffer end click up: {}:{}", self.position_on_line, s);
                            }
                        }
                        log!(self, "   ");
                    }
                    Key::Down => {
                        let line = self.state.hist.get_next();
                        if !line.is_empty() {
                            self.clear_line_and_buffer();
                            self.display_line(&line);
                        }
                    }
                    Key::Right => self.move_right(),
                    Key::Left => self.move_left(),
                    Key::Ctrl_C => {
                        //TODO: should interrupt currently running process
                        _ = self.state.hist.save();
                        self.clear_line_and_buffer();
                    }
                    Key::Ctrl_Z => std::process::exit(1),
                    Key::Backspace => {
                        if self.position_on_line == 0 {
                            continue;
                        }
                        if self.position_on_line == self.buffer.len() {
                            self.delete_one_char();
                            continue;
                        }
                        self.position_on_line -= 1;
                        self.buffer.remove(self.position_on_line);
                        move_left_no_check();
                        display_buffer(&self.buffer[self.position_on_line..]);
                        eprint!(" "); // delete the last char from the displayed line
                        self.move_cursor_left(self.buffer.len() - self.position_on_line + 1);
                        // go back to the correct position
                    }
                    Key::Ctrl_Left => {
                        self.go_to_previous_word();
                    }
                    Key::Ctrl_Right => {
                        self.go_to_next_word();
                    }
                    Key::Home => {
                        self.go_to_beginning_of_line();
                    }
                    Key::End => {
                        self.go_to_end_of_line();
                    }
                    Key::Char(c) => {
                        log!(self, " received ({}->{})", c, c as char);
                        if self.position_on_line == self.buffer.len() {
                            self.add_one_char(c);
                        } else {
                            self.add_one_char_at_current_pos(c);
                        }
                    }
                    Key::Nop | Key::Ctrl_Up | Key::Ctrl_Down => {}
                    k => unimplemented!("{:?} not handled", k),
                }
                stdout.flush().expect("unable to flush stdout");
            }
        }
    }

    fn run_cmd(&mut self) {
        eprintln!();
        if let Ok(s) = std::str::from_utf8(&self.buffer) {
            log!(self, "buffer from cmd: {}", s);
        }

        if self.buffer.len() > 0 {
            let line = &self.buffer.to_owned();
            let line = std::str::from_utf8(line).unwrap();
            if let Err(err) = self._run_cmd(line) {
                eprintln!("io error: {}", err);
            } else {
                self.state.hist.add(line.to_owned());
            }
        }

        self.buffer.clear();
        self.position_on_line = 0;
    }

    fn _run_cmd(&mut self, line: &str) -> Result<(), std::io::Error> {
        let args: Vec<&str> = line.split(' ').filter(|s| !s.is_empty()).collect();
        if let Some(func) = self.state.builtins.get(args[0]) {
            func(args, &mut self.state)?;
        } else {
            let output = run_cmd_line(&line)?;
            let stdout = std::str::from_utf8(&output.stdout).unwrap();
            let stderr = std::str::from_utf8(&output.stderr).unwrap();
            eprint!("{}{}", stderr, stdout);
        }
        Ok(())
    }

    fn display_line(&mut self, chars: &str) {
        if chars.is_empty() {
            return;
        }
        self.add_multiple_chars(&chars.as_bytes());
    }

    fn handle_completion(&mut self) {
        eprintln!("todo! handle autocompletion")
    }

    fn rm_n_chars(&mut self, n: usize) {
        let go_left = format!("\x1b[{n}D");
        eprint!("{}{}{}", go_left, " ".repeat(n), go_left);
    }

    fn delete_one_char(&mut self) {
        eprint!("{MOVE_LEFT} {MOVE_LEFT}");
        self.position_on_line -= 1;
        self.buffer.pop();
    }

    fn add_multiple_chars(&mut self, chars: &[u8]) {
        self.position_on_line += chars.len();
        self.buffer.extend_from_slice(chars);
        display_buffer(&chars);
    }

    fn add_one_char_at_current_pos(&mut self, c: u8) {
        // assuming that the user is not writing at the end of the buffer
        // must only be called when adding chars at the middle of a line
        let idx = self.position_on_line;
        self.buffer[idx..].rotate_right(1);
        self.buffer.push(self.buffer[idx]);
        self.buffer[idx] = c;
        // redraw the line
        move_right_no_check();
        // refresh line requires a coherent internal state
        self.position_on_line += 1;
        self.refresh_line();
        self.position_on_line = idx + 1;
    }

    fn clear_line_and_buffer(&mut self) -> usize {
        let diff = self.clear_line();
        self.buffer.clear();
        return diff;
    }

    fn clear_line(&mut self) -> usize {
        let pos = self.position_on_line;
        let len = self.buffer.len();
        if pos > 0 {
            eprint!("\x1b[{pos}D{CLR_EOL}");
        } else {
            eprint!("{CLR_EOL}");
        }
        self.position_on_line = 0;
        return len - pos;
    }

    fn refresh_line(&mut self) {
        let diff = self.clear_line();
        display_buffer(&self.buffer);
        if diff > 0 {
            // display moves the cursor len to the right
            // must go back to the left without changing internal state
            eprint!("\x1b[{diff}D");
        }
    }

    fn add_one_char(&mut self, c: u8) {
        self.buffer.push(c);
        self.position_on_line += 1;
        eprint!("{}", c as char);
    }

    fn new() -> Self {
        let position_on_line = 0;
        let buffer: Vec<u8> = Vec::new();
        let state = super::ShellState::new();
        let raw_tty = tty::RawTTy::new();
        // to remove afer debug
        let debug_file = std::fs::File::create("/tmp/debug_shell").unwrap();

        Self {
            state,
            position_on_line,
            tty: raw_tty,
            buffer,
            debug_file,
        }
    }
    fn go_to_previous_word(&mut self) {
        if self.position_on_line == 0 {
            return;
        }
        let mut pos = self.position_on_line - 1;
        // jump all spaces
        while pos > 0 && self.buffer[pos] == ' ' as u8 {
            pos -= 1;
        }
        while pos > 0 && self.buffer[pos] != ' ' as u8 {
            pos -= 1;
        }
        let diff = self.position_on_line - pos;
        eprint!("\x1b[{diff}D");
        self.position_on_line = pos;
    }
    fn go_to_next_word(&mut self) {
        let len = self.buffer.len();
        if self.position_on_line == len {
            return;
        }
        let mut pos = self.position_on_line;
        // jump all spaces
        while pos < len && self.buffer[pos] == ' ' as u8 {
            pos += 1;
        }
        // jump a word
        while pos < len && self.buffer[pos] != ' ' as u8 {
            pos += 1;
        }
        let diff = pos - self.position_on_line;
        eprint!("\x1b[{diff}C"); //move cursor right
        self.position_on_line = pos;
    }

    fn go_to_beginning_of_line(&mut self) {
        if self.position_on_line > 0 {
            eprint!("\x1b[{}D", self.position_on_line); //move cursor left
            self.position_on_line = 0;
        }
    }

    fn go_to_end_of_line(&mut self) {
        let diff = self.buffer.len() - self.position_on_line;
        if diff > 0 {
            eprint!("\x1b[{diff}C"); // move cursor right
            self.position_on_line += diff;
        }
    }

    fn move_left(&mut self) {
        if self.position_on_line > 0 {
            self.position_on_line -= 1;
            eprint!("{MOVE_LEFT}");
        }
    }

    fn move_cursor_left(&self, n: usize) {
        for _ in 0..n {
            move_left_no_check()
        }
    }

    fn move_right(&mut self) {
        if self.position_on_line + 1 < self.buffer.len() {
            move_right_no_check();
            self.position_on_line += 1;
        }
    }
}

const CLR_EOL: &str = "\x1b[K";
const MOVE_LEFT: &str = "\x1b[D";
const MOVE_RIGHT: &str = "\x1b[C";

struct Repl {
    state: super::ShellState,
    position_on_line: usize,
    // repl is a singleton with exclusive access to stdin
    tty: RawTTy<'static>,

    buffer: Vec<u8>, // characters read so far
    debug_file: std::fs::File,
}

// must print in stderr or stdout only
// display bugs happen when mixing both
fn move_right_no_check() {
    eprint!("{MOVE_RIGHT}");
}
fn move_left_no_check() {
    eprint!("{MOVE_LEFT}");
}

fn display_buffer(buffer: &[u8]) {
    if let Ok(s) = std::str::from_utf8(buffer) {
        eprint!("{}", s);
    } else {
        unreachable!("repl does only supports UTF-8 encoding");
    }
}
