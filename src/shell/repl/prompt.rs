use std::time::SystemTime;

use super::colors;

pub struct Prompt {
    primary_color: &'static str,
    uname_color: &'static str,
    text_color: &'static str,
    time_color: &'static str,
    elapsed_color: &'static str,
    folder_color: &'static str,
    username: String,
}

impl Prompt {
    pub fn new() -> Self {
        let mut primary_color = colors::GREEN;
        let text_color = colors::DEFAULT;
        let time_color = colors::RED;

        let folder_color = colors::WHITE;
        let elapsed_color = colors::WHITE;

        let mut username = std::env::var("USER").expect("USER must be defined");

        let mut uname_color = colors::WHITE;

        if username == "root" {
            uname_color = colors::BLINK_RED;
            primary_color = colors::WHITE;
            username = format!("{}{}", username, colors::NO_BLINK);
        }

        Self {
            folder_color,
            primary_color,
            uname_color,
            text_color,
            time_color,
            elapsed_color,
            username,
        }
    }
    pub fn show(&self, folder: &str, elapsed_seconds: u64) {
        eprint!(
            "\n{}{}",
            self.text_color,
            self.compute(folder, elapsed_seconds)
        );
    }
    pub fn compute(&self, folder: &str, elapsed_seconds: u64) -> String {
        let time = self.get_hours_minutes();

        let primary_color = &self.primary_color;
        let uname_color = &self.uname_color;
        let text_color = &self.text_color;
        let folder_color = &self.folder_color;
        let username = &self.username;
        let first_line = format!(
            "{primary_color}┌────{uname_color}{username}{primary_color}> {time} {folder_color}{folder}\n",
        );

        let elapsed_time = self.get_elapsed_time(elapsed_seconds);
        let second_line = format!("{primary_color}└──{elapsed_time}{primary_color}──❯{text_color}");

        format!("{}{} ", first_line, second_line)
    }

    fn get_hours_minutes(&self) -> String {
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        let secs_since_midnight: u32 = (now % SECS_IN_A_DAY as u64) as u32;
        let current_hour = secs_since_midnight / SECS_IN_A_HOUR + 2; // utc +2
        let current_minute = (secs_since_midnight % SECS_IN_A_HOUR) / SECS_IN_A_MIN;

        format!(
            "{}{}:{}",
            self.time_color,
            current_hour,
            num_to_string(current_minute)
        )
    }

    fn get_elapsed_time(&self, elapsed_seconds: u64) -> String {
        if elapsed_seconds < 3 {
            return "".to_owned();
        }
        let time_color = &self.elapsed_color;

        if elapsed_seconds > 60 {
            let mins = elapsed_seconds / 60;
            let secs = elapsed_seconds % 60;
            let srep = if secs < 10 {
                format!("0{}", secs)
            } else {
                format!("{}", secs)
            };
            return format!("{time_color}{mins}m{srep}s");
        }

        return format!("{time_color}{elapsed_seconds}s");
    }
}

fn num_to_string(nbr: u32) -> String {
    if nbr < 10 {
        format!("0{}", nbr)
    } else {
        format!("{}", nbr)
    }
}

const SECS_IN_A_DAY: u32 = 86400;
const SECS_IN_A_HOUR: u32 = 3600;
const SECS_IN_A_MIN: u32 = 60;
