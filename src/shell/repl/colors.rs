pub const RED: &'static str = "\x1b[31;1m";
pub const BLINK_RED: &'static str = "\x1b[31;5;1m";

pub const GREEN: &'static str = "\x1b[32;1m";
// pub const YELLOW: &'static str = "\x1b[33;1m";
pub const WHITE: &'static str = "\x1b[37;1m";
pub const DEFAULT: &'static str = "\x1b[0m";
pub const NO_BLINK: &'static str = "\x1b[25m";
