#![macro_use]
use core::mem::MaybeUninit;
use std::io;

use super::log;
use std::io::Write;

use crate::libc;
use std::io::Read;

#[allow(non_camel_case_types)]
#[derive(Debug)]
pub enum Key {
    Nop,
    Up,
    Down,
    Left,
    Right,
    Home,
    Tab,
    Backspace,
    PageUp,
    PageDown,
    Insert,
    End,
    Suppr,
    Enter,
    Ctrl_C,
    Ctrl_Z,
    Ctrl_Up,
    Ctrl_Down,
    Ctrl_Left,
    Ctrl_Right,
    //    Text(String) // to parse mutiline copy paste, will be associated with a bultin
    Char(u8),
}

// terminal that only supports VT100 escape sequences
pub struct RawTTy<'a> {
    stdin: io::StdinLock<'a>,
    buffer: [u8; 6],
    buf_pos: usize,
    remaining: usize,
    debug_file: std::fs::File,
}

macro_rules! tdb {
    ($x:ident) => {{
        reset_terminal_mode();
        todo!("key code {:?}", $x)
    }};
    ($x:ident, $s:tt) => {{
        reset_terminal_mode();
        todo!("{} received key code {:?}", $s, $x)
    }};
}

const BUFFER_SIZE: usize = 6;

impl<'a> RawTTy<'a> {
    pub fn new() -> Self {
        setup_raw_terminal();
        let debug_file = std::fs::File::create("/tmp/debug_shell_tty").unwrap();

        Self {
            stdin: io::stdin().lock(),
            buffer: [0u8; BUFFER_SIZE],
            buf_pos: 0,
            remaining: 0,
            debug_file,
        }
    }

    pub fn read_one_key(&mut self) -> Key {
        //blocks until a key is pressed
        //must be called after setup_raw_terminal
        if self.remaining != 0 {
            // consume buffered key codes, if correctly implemented they are chars
            let mut res = self.buffer[self.buf_pos];

            if res == 13 {
                // 'enter' key code must be replaced with newline
                res = 10;
            }
            log!(self, "-----------------------------");
            log!(
                self,
                "sending index {}:[{}] from buffer {:?}",
                self.buf_pos,
                res,
                self.buffer
            );
            log!(self, "corresponds to char {}", res as char);
            log!(self, "-----------------------------");
            self.remaining -= 1;
            self.buf_pos += 1;
            return Key::Char(res);
        }

        let size = self
            .stdin
            .read(&mut self.buffer)
            .expect("unable to read from terminal");

        let buf = self.buffer;
        //eprintln!("{:?}\n", self.buffer);
        if size == 1 {
            return match buf[0] {
                9 => Key::Tab,
                13 => Key::Enter,
                127 => Key::Backspace,
                3 => Key::Ctrl_C,
                26 => Key::Ctrl_Z,
                c => Key::Char(c),
            };
        }
        // if first char is not espace code buffer must
        // be treated like an array of printable chars
        if buf[0] != 27 {
            log!(self, "buffering {}", self.remaining);
            log!(self, "############################################");
            if self.buffer[0] == 13 {
                // 'enter key' code must be replaced with newlines
                self.buffer[0] = 10;
            }
            // first char is already processed in this loop
            // must only consume remaining size -1 chars
            self.remaining = size - 1;
            self.buf_pos = 1;
            return Key::Char(buf[0]);
        }

        if size == 3 {
            return match buf[2] {
                65 => Key::Up,
                66 => Key::Down,
                67 => Key::Right,
                68 => Key::Left,
                70 => Key::End,
                72 => Key::Home,
                _ => tdb!(buf),
            };
        }

        if size != 6 {
            // could return a no op key in release mode
            return tdb!(buf, "so far all specials keys had 3 or 6 bytes");
        }

        if buf[2] == 126 {
            return match buf[1] {
                50 => Key::Insert,
                51 => Key::Suppr,
                53 => Key::PageUp,
                54 => Key::PageDown,
                _ => tdb!(buf),
            };
        }
        if buf[2] == 49 && buf[3] == 59 && buf[4] == 53 {
            return match buf[5] {
                65 => Key::Ctrl_Up,
                66 => Key::Ctrl_Down,
                67 => Key::Ctrl_Right,
                68 => Key::Ctrl_Left,
                70 | 72 | 126 => Key::Nop,
                _ => tdb!(buf),
            };
        };
        // uncomment when trying to implement new keys codes
        // tdb!(buf);
        Key::Nop
    }
}

pub fn identify_key_code() {
    let mut raw_tty = RawTTy::new();
    loop {
        let key = raw_tty.read_one_key();
        if let Key::Char(c) = key {
            if c == 'q' as u8 {
                break;
            }
        }
    }
}

pub struct Size {
    pub width: u16,
    pub height: u16,
}

fn error_msg(msg: &'static str) {
    println!("libc error: {}", msg)
}

pub fn get_terminal_size() -> Size {
    let ws = unsafe {
        let mut ptr: MaybeUninit<libc::Winsize> = MaybeUninit::uninit();
        let ret = libc::ioctl(libc::STDOUT_FILENO, libc::TIOCGWINSZ, ptr.as_mut_ptr());
        if ret != 0 {
            error_msg("unable to read terminal size, will use default value");
            return Size {
                width: 94,
                height: 26,
            };
        }
        ptr.assume_init()
    };
    Size {
        width: ws.ws_col,
        height: ws.ws_row,
    }
}

pub fn reset_terminal_mode() {
    // magic values extracted via a println("{:?}", termios)
    // corresponds to a standard terminal mode flags
    let mut termios = libc::Termios {
        c_iflag: 17664,
        c_oflag: 5,
        c_cflag: 191,
        c_lflag: 35387,
        c_line: 0,
        c_cc: [
            3, 28, 127, 21, 4, 0, 1, 0, 17, 0, 26, 0, 18, 15, 23, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0,
        ],
    };

    unsafe {
        let fd = get_stdin_fd();
        if libc::tcsetattr(fd, libc::TCSADRAIN, &mut termios) != 0 {
            error_msg("unable to reset terminal to normal mode");
        }
    }
}

fn setup_raw_terminal() {
    unsafe {
        let fd = get_stdin_fd();
        let mut ptr = MaybeUninit::uninit();

        if libc::tcgetattr(fd, ptr.as_mut_ptr()) != 0 {
            error_msg("unable to get attributes of the terminal");
            return;
        }

        let mut termios = ptr.assume_init();
        let c_oflag = termios.c_oflag;

        libc::cfmakeraw(&mut termios);
        termios.c_oflag = c_oflag;

        if libc::tcsetattr(fd, libc::TCSADRAIN, &mut termios) != 0 {
            error_msg("unable to set terminal in raw mode");
        }
    }
}

unsafe fn get_stdin_fd() -> libc::int {
    use std::os::unix::io::AsRawFd;
    if libc::isatty(libc::STDIN_FILENO) == 1 {
        // interactive sessions will always have an stdin
        libc::STDIN_FILENO
    } else {
        let tty = std::fs::File::open("/dev/tty").expect("unable to open tty");
        tty.as_raw_fd()
    }
}
