use std::collections::HashMap;

pub struct Trie {
    children: TChild,
}

// TODO: use a custom array based hashmap if perf issues
type TChild = HashMap<u8, TNode>;

struct TNode {
    priority: u32,
    children: TChild,
}

impl Trie {
    pub fn new() -> Self {
        Self {
            children: TChild::new(),
        }
    }
    pub fn get(&self, buff: &[u8]) -> Vec<String> {
        let mut res = Vec::new();
        let mut prio: Vec<u32> = Vec::new();
        // dfs(self.children, , , )
        if !self.children.contains_key(&buff[0]) {
            return res;
        }
        let mut node = &self.children[&buff[0]];
        for i in 1..buff.len() {
            if !node.children.contains_key(&buff[i]) {
                return res;
            }
            node = &node.children[&buff[i]];
        }
        dfs(node, buff.to_vec(), &mut prio, &mut res);
        let n = &prio.len();
        quicksort(&mut res, &mut prio, 0, n - 1);
        return res;
    }

    pub fn insert(&mut self, buff: &[u8], priority: u32) {
        // doesn't handle single char correctly
        if !self.children.contains_key(&buff[0]) {
            self.children.insert(buff[0], TNode::new_intermediary());
        }
        let mut node: &mut TNode = &mut self.children.get_mut(&buff[0]).unwrap();
        for i in 1..buff.len() {
            if !node.children.contains_key(&buff[i]) {
                node.children.insert(buff[i], TNode::new_intermediary());
            }
            node = node.children.get_mut(&buff[i]).unwrap();
        }
        node.priority = priority;
    }
}

impl TNode {
    fn new_intermediary() -> Self {
        Self {
            priority: 0,
            children: HashMap::new(),
        }
    }
    fn new_final_value(priority: u32) -> Self {
        Self {
            priority,
            children: HashMap::new(),
        }
    }
}

fn dfs(node: &TNode, prefix: Vec<u8>, prio: &mut Vec<u32>, words: &mut Vec<String>) {
    for k in node.children.keys() {
        let mut v = prefix.clone();
        node.children.get(k);
        v.push(*k);
        let newnode = &node.children[k];
        dfs(newnode, v, prio, words);
    }
    if node.priority > 0 {
        let s = std::string::String::from_utf8(prefix).expect("not handling non utf8 in history");
        prio.push(node.priority);
        words.push(s);
    }
}

// descending order
fn quicksort(words: &mut Vec<String>, prio: &mut Vec<u32>, ll: usize, rr: usize) {
    if ll >= rr {
        return;
    }
    let mut l = ll;
    let mut r = rr;
    let pivot = prio[(l + r) / 2];
    loop {
        while prio[l] > pivot {
            l += 1;
        }
        while prio[r] < pivot {
            r -= 1;
        }
        if l >= r {
            break;
        }
        let tmp = prio[r];
        prio[r] = prio[l];
        prio[l] = tmp;
        let s = std::mem::take(&mut words[l]);
        let t = std::mem::take(&mut words[r]);
        words[l] = t;
        words[r] = s;
        l += 1;
        r -= 1;
    }
    quicksort(words, prio, ll, r);
    quicksort(words, prio, r + 1, rr);
}

#[cfg(test)]
mod tests {
    use insta;

    use super::*;
    trie_get_test!(hello);
    trie_get_test!(he);
    trie_get_test!(ha);

    macro_rules! trie_get_test {
        ($name:ident) => {
            #[test]
            fn $name() {
                let mut settings = insta::Settings::clone_current();
                let mut trie = Trie::new();
                let words = vec![
                    "hello wordlast",
                    "hello word1",
                    "hello ",
                    "hella ",
                    "hello a b c",
                    "hello word1",
                    "hello word0",
                ];
                words
                    .iter()
                    .enumerate()
                    .for_each(|(i, s)| trie.insert(s.as_bytes(), (i + 1) as u32));

                settings.set_description(
                    "must extract all matching values given prefix ranked by priority",
                );
                settings.set_snapshot_path("../../../../tests/snapshots");
                settings.bind(|| {
                    let $name = trie.get(stringify!($name).as_bytes());
                    insta::assert_snapshot!(Matching($name));
                });
            }
        };
    }

    pub(crate) use trie_get_test;

    struct Matching(Vec<String>);
    use std::fmt::{Display, Error, Formatter};
    impl std::fmt::Display for Matching {
        fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
            writeln!(
                f,
                "{}",
                // self.s.iter().fold(String::new(), |acc, arg| acc + arg)
                self.0
                    .iter()
                    .fold(String::new(), |acc, arg| acc + arg + ", ")
            )
        }
    }
}
