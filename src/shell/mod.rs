pub mod builtins;
pub mod cmd;
pub mod repl;

pub struct ShellState {
    pwd: String,
    home: String,
    oldpwd: String,
    hist: repl::history::History,
    builtins: builtins::NameToFunc,
}

impl ShellState {
    pub fn new() -> Self {
        let home = std::env::var("HOME").expect("HOME must be defined");
        let path = format!("{home}/.cache/.hist.irsh");
        let hist = repl::history::History::new(path);
        let builtins = builtins::init();
        let pwd = match std::env::current_dir() {
            Ok(p) => format!("{}", p.display()),
            Err(_) => home.to_owned(),
        };

        Self {
            pwd,
            oldpwd: home.clone(),
            hist,
            home,
            builtins,
        }
    }
}
