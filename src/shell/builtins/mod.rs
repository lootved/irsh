use std::collections::HashMap;
use std::io::Error;
// first argument will always be the builtin name
// should be ignored
use super::cmd;
use crate::shell::repl::tty;

type Func = fn(Vec<&str>, &mut State) -> Result<(), std::io::Error>;

type State = super::ShellState;

pub type NameToFunc = HashMap<String, Func>;

pub type BuiltinRet = Result<(), Error>;

macro_rules! add_builtin {
    ($a:expr,$b:ident) => {
        $a.insert(stringify!($b).to_owned(), $b)
    };
}

pub fn init() -> NameToFunc {
    let mut res = NameToFunc::new();
    //not possible to do a loop as the loop index
    //is stringified leading to a single value in the map
    add_builtin!(res, cd);
    add_builtin!(res, pwd);
    add_builtin!(res, exit);
    add_builtin!(res, echo);
    add_builtin!(res, reset);
    add_builtin!(res, reset);
    add_builtin!(res, ls);
    res.insert("..".to_owned(), dotdot);
    res
}

fn cd(args: Vec<&str>, state: &mut super::ShellState) -> BuiltinRet {
    //handle special cases
    if args.len() < 2 {
        let old_path = std::mem::take(&mut state.pwd);
        std::env::set_current_dir(&state.home)?;
        state.pwd = state.home.clone();
        state.oldpwd = old_path;
        return Ok(());
    } else if args[1] == "-" {
        let old_path = std::mem::take(&mut state.pwd);
        std::env::set_current_dir(&state.oldpwd)?;
        state.pwd = state.oldpwd.clone();
        state.oldpwd = old_path;
        return Ok(());
    }

    let mut _path = args[1];
    let mut path = String::new();
    if !_path.starts_with("/") {
        path.push_str(&state.pwd);
        path.push_str("/");
    }
    path.push_str(_path);
    // path is now absolute

    let sub_paths: Vec<&str> = path.split("/").collect();
    let mut ignore_next = 0;
    let mut idx = sub_paths.len() - 1;
    let mut sub_result = Vec::new();
    // path[0] == ""
    while idx > 0 {
        if sub_paths[idx] == ".." {
            ignore_next += 1;
        } else {
            if ignore_next > 0 {
                ignore_next -= 1;
            } else {
                let a = sub_paths[idx];
                sub_result.push(sub_paths[idx]);
            }
        }
        idx -= 1;
    }
    let mut res = String::new();
    let len = sub_result.len();
    for i in 0..len {
        res.push_str("/");
        res.push_str(sub_result[len - i - 1]);
    }

    std::env::set_current_dir(&res)?;
    let old_path = std::mem::take(&mut state.pwd);
    state.pwd = res;
    state.oldpwd = old_path;
    Ok(())
}

fn echo(args: Vec<&str>, _state: &mut super::ShellState) -> BuiltinRet {
    let mut res = String::new();
    // skip echo passed as arg
    for arg in args.iter().skip(1) {
        res.push_str(arg);
        res.push_str(" ");
    }
    res.pop();
    println!("{}", res);
    Ok(())
}

fn ls(args: Vec<&str>, _state: &mut super::ShellState) -> BuiltinRet {
    let mut lsargs = vec!["-h", "--color=auto", "--group-directories-first"];
    for arg in args.into_iter().skip(1) {
        lsargs.push(arg);
    }
    cmd::run_no_redirect("/bin/ls", lsargs)
}

fn dotdot(_args: Vec<&str>, state: &mut super::ShellState) -> BuiltinRet {
    let cdargs = vec!["cd", ".."];
    cd(cdargs, state)
}

fn pwd(_args: Vec<&str>, state: &mut super::ShellState) -> BuiltinRet {
    println!("{}", &state.pwd);
    Ok(())
}

fn exit(args: Vec<&str>, _state: &mut super::ShellState) -> BuiltinRet {
    if args.is_empty() {
        std::process::exit(0)
    }
    let code: i32 = args[0].parse().unwrap_or_else(|_| 1);
    std::process::exit(code)
}

fn reset(_args: Vec<&str>, _state: &mut super::ShellState) -> BuiltinRet {
    // TODO should fully clear the state and reset it
    let size = tty::get_terminal_size();
    println!("{} lines and {} columns", size.height, size.width);
    Ok(())
}
