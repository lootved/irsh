macro_rules! snap {
    ($name:tt, $desc: tt, $closure:tt ) => {
        #[test]
        fn $name() {
            let path = format!("tests/inputs/runtime/{}.sh", stringify!($name));
            let program = crate::compiler::tests::compile(&path).unwrap();
            let evaluated_variables = $closure(program);

            let mut settings = insta::Settings::clone_current();
            settings.set_description($desc);
            settings.set_snapshot_path("../../tests/snapshots");
            settings.bind(|| {
                insta::assert_snapshot!(evaluated_variables);
            });
        }
    };
}

pub(crate) use snap;
