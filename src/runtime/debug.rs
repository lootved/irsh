use super::eval::Evaluator;
use crate::compiler::debug;
impl ToString for Evaluator {
    fn to_string(&self) -> String {
        let mut res = debug::prog_to_string(
            &self.functions,
            &self.consts,
            &self.expr,
            &self.name_to_var,
            &self.name_to_func,
        );

        res.push_str("\n---- global vars: idx->value ----\n");

        for idx in 0..self.globs.len() {
            let val = format!("{}->{}\n", idx, self.globs[idx]);
            res.push_str(&val);
        }

        res
    }
}
