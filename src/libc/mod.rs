#![allow(non_camel_case_types)]
pub type tcflag_t = u32;
pub type ushort = u16;

pub type int = i32;
pub type ulong = u64;

#[repr(C)]
pub struct Termios {
    pub c_iflag: tcflag_t,
    pub c_oflag: tcflag_t,
    pub c_cflag: tcflag_t,
    pub c_lflag: tcflag_t,
    pub c_line: u8,
    pub c_cc: [u8; 32],
}
extern "C" {
    pub fn isatty(fd: i32) -> i32;
    pub fn tcgetattr(fd: i32, termios: *mut Termios) -> i32;
    pub fn tcsetattr(fd: i32, optional_actions: i32, termios: *const Termios) -> i32;
    pub fn cfmakeraw(termios: *mut Termios);

    pub fn ioctl(fd: int, request: ulong, ...) -> int;

}

#[repr(C)]
pub struct Winsize {
    // struct must be 64 bits long
    // according to ioctl doc
    pub ws_row: ushort,
    pub ws_col: ushort,
    _ignore: u32,
}

pub const TIOCGWINSZ: ulong = 0x5413;

pub const STDIN_FILENO: int = 0;
pub const STDOUT_FILENO: int = 1;
pub const TCSADRAIN: int = 1;
