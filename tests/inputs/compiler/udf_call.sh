#!/bin/bash

GLOB1="from global"

function fn1_opt() {
	_loc="${1}"
	_loc2="${2:-world}"

	echo "${_loc} ${_loc2}" "${GLOB1}"
}

function main() {
	fn1_opt hello
}
