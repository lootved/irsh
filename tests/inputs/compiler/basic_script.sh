#!/bin/bash

GLOB1="global_var"
GLOB_2="glob_2"
export GLOB_3="${GLOB1}-${GLOB_2}"
GLOB_2="glob_or_3"

function fn_0() {
	echo "func with no args"
}

function fn1_opt() {
	_loc="${1:-hello}"
	echo 'func with optional args: ${_loc}'
}

function fn_1() {
	_loc="${1}"
	echo 'func with one arg ${_loc}'
}

function fn4_opt() {
	_loc1="${1}" # 1 mandatory arg
	_loc2="${2:-arg2}"
	_loc3="${3:-arg3}"
	_loc4="${4:-arg4}"

	echo 'func with optional args: ${_loc1} ${_loc2} ${_loc3} ${_loc4}'
}

function main() {
	fn_0
	fn1_opt
	fn1_opt "func with no args"
}
