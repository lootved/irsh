VAR_0="hello"
VAR_1="from"
VAR_2="bash"
VAR_3="${VAR_0} ${VAR_1}"
VAR_4="${VAR_3} ${VAR_2} ${VAR_1} ${VAR_1}"

function update_var() {
	_v1="${1}"
	VAR_4="${VAR_3} ${_v1}${VAR_2}"

}

function update_var2() {
	# see what happens when glob is updated inside two methods
	VAR_4="${VAR_3} ${VAR_2}"

}

function main() {
	update_var s

}
