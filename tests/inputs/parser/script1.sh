#!/bin/bash

glob="world"

function say() {
	_loc="${1:-hello}"
	# should be ignored
	_res="${_loc} ${glob}" # should be ignored

	echo "${_res}" | cut -d" " -f1
	echo 'single quoted string ${_loc} &> must not be expanded' &>/dev/null
}

# bash would normally expose both _loc and _res variables to the calling shell
say &>/dev/null

res1="$(say hellow)"
res2="$(echo hello-world | cut -d- -f2)"

echo "should print hellow world: ${res1} ${res2}"
echo "should print hello :" "$(say)"

echo "should fail as trying to access a local variable of method say: ${_loc}"
echo "should also report the error and not print this message"
# TODO one test for each functional use case
