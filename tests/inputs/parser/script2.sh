#!/bin/bash

GLOB="global_var"

function say() {
	#_loc="${1:-hello}"
	_loc="hello"
	#_res="${_loc} ${GLOB}"

	echo "${_loc}" world "${GLOB}"
	#echo "${_res}" | cut -d" " -f1

	# echo 'single quoted string ${_loc} &> must not be expanded' &>/dev/null
	echo 'single quoted string ${_loc} &> must not be expanded' /dev/null

	# echo "${loc}" # this command should fail
}
USELESS_VAR="useless value with ${GLOB}"
#USELESS_VAR="$(echo hello world)"
echo say "${GLOB}"
say world
