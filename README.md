# SBash

## Description
  Strict Bash Shell, compiler and interpreter.
  Designed to be used as a Linux interactive shell with minimal dependencies,
  and compile bash script to standalone static binaries.
  Will only support a subset of bash syntax.

## TODO
- when move hist up or down, allign cursor with end of chars, bugs when different lenghts
- eval in repl mode
- builtins in repl mode
- source external file
- implement alias
- glob expansion starting with `*`
- chain commands with pipes
- assign command output to variable
- use a single model for commands/builtins/functions output
- implement redirections
- implement if/for block
- finish autocompletion
- copy from the terminal output of previous command
- step by step execution with depth control, display command and wait
- option to automatically redirect output of commands to a file

## Features
- not a posix shell
- fast compilation
- statically compiled with no dependencies
- support a unique bash syntax for variable/function declaration
- cannot override a function, first definition
- local variables (starting with a lowercase) are not propagated outside the function
- function arguments with no default value are considered mandatory
- error when calling a function without enough mandatory args
- error when trying to access undefined variables
- always assume a { after a $ and that the brackets/parenthesis are correctly closed
- one level of sub-command only
- sub-commands can only be used at the beginning of double quoted string
- sub-commands are limited to variables/words and redirection symbols
- no support (yet?) for multi-lines strings
- variables must always be quoted
- builtins implemented as simple rust functions (+maybe macro to simplify?)
- no external config or controllable hidden variables
- functions cannot create new global variables
- function arguments must be assigned to local variables at the beginning of the func
- execution starts from `main` function

## Limitations
- can fail on some edge cases
- no support for nested quotes
- no redirections yet
- support only ${alpha_or_num}, no $@ or $* or any other form
- max 9 args passed to user defined functions
- no if/while/for yet
- no math yet, will probably be implemented as builtins if needed

## Objectives
- parse bash variables with scoping and defaults
- parse bash functions via `function` keyword
- support invocation of external commands and functions
- implement a subset of basic bash functionalities
- support stdout/err redirections and piping


## Build
```bash
    cargo build --target x86_64-unknown-linux-musl --release
```

## Test
```bash
    cargo test # updates the snapshots
    cargo insta review # review updated snapshots
    cargo insta accept # accept all updates
```

## Other
    fix some warnings
```bash
    cargo fix --allow-dirty
```



